/**
 * Created by Dmitry on 7/17/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

public class ImageViewerMod extends JFrame {
    private JPanel imagePanel;
    private JLabel imageLabel;
    private JFileChooser fileChooser;

    private JPanel imageNamePanel;
    private JLabel imageNameLabel;

    private JMenuBar menuBar;
    private JMenu fileMenu;

    private JMenuItem exitItem;
    private JMenuItem buttonItem;

    public ImageViewerMod() {
        this.setTitle("Image Viewer");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());

        buildImagePanel();
        buildMenuBar();
        buildImageNamePanel();
        this.setJMenuBar(menuBar);

        this.add(imagePanel, BorderLayout.CENTER);
        this.add(imageNamePanel, BorderLayout.SOUTH);

        fileChooser = new JFileChooser(".");
        this.pack();
        this.setVisible(true);
    }

    private void buildImagePanel() {
        imagePanel = new JPanel();
        imageLabel = new JLabel("Click file menu to open an image.");
        imagePanel.add(imageLabel);
    }

    private void buildImageNamePanel() {
        imageNamePanel = new JPanel();
        imageNameLabel = new JLabel("");
        imageNamePanel.add(imageNameLabel);
    }

    private void buildMenuBar() {
        menuBar = new JMenuBar();
        buildFileMenu();
        menuBar.add(fileMenu);
    }

    private void buildFileMenu() {
        fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);

        buttonItem = new JMenuItem("Open");
        buttonItem.setMnemonic(KeyEvent.VK_O);
        buttonItem.addActionListener(new ButtonListener());

        exitItem = new JMenuItem("Exit");
        exitItem.setMnemonic(KeyEvent.VK_X);
        exitItem.addActionListener(new ExitListener());

        fileMenu.add(buttonItem);
        fileMenu.add(exitItem);
    }

    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            File selectedFile;
            ImageIcon image;
            String filename;
            int fileChooserStatus;

            fileChooserStatus = fileChooser.showOpenDialog(ImageViewerMod.this);

            if (fileChooserStatus == JFileChooser.APPROVE_OPTION) {
                selectedFile = fileChooser.getSelectedFile();
                filename = selectedFile.getPath();
                image = new ImageIcon(filename);
                imageLabel.setIcon(image);
                imageLabel.setText(null);
                imageNameLabel.setText(selectedFile.getName());
                pack();
            }
        }
    }

    private class ExitListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            System.exit(0);
        }
    }


    public static void main(String[] args) {
        new ImageViewerMod();
    }
}
